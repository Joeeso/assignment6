

public class CheckSSN {


  public boolean check_ssn(String s) {
    for (int i=0; i < s.length(); i++) {
      char c = s.charAt(i);
      if (!Character.isDigit(c) && c != '-')
        return false;
    }
    return true;
  }


}
