
public class TestSuite1 {


  public static boolean check_ssn(String s) {
    for (int i=0; i < s.length(); i++) {
      char c = s.charAt(i);
      if (!Character.isDigit(c) && c != '-')
        return false;
    }
    return true;
  }


		public static void main(String[] args) {
			
			String s1 = "234-23-9045";
			System.out.println(check_ssn(s1));
			
			String s2 = "234.35-9045";
			System.out.println(check_ssn(s2));
			
			String s3 = "234/23/9045";
			System.out.println(check_ssn(s3));
			
			String s4 = "(234)56-9045";
			System.out.println(check_ssn(s4));
			
			String s5 = "234_23_9045";
			System.out.println(check_ssn(s5));

		}

	
  
}
